+++
title = "My Linux Journey"
date = 2020-08-07
tags = "distros"
description = "I fall down and got up pretty frequently in my early days with Linux. All those experiences thought me hoe to use a computer properly. Today I have Linux servers running 24/7 my phone backup, laptop sync etc."
status = "published"
comment = "https://fosstodon.org/@murtezayesil/104648575119352342"
hundreddaystooffload = "FizzBuzz (15)"
+++

I learned to use linux by breaking it. First time I broke Ubuntu, I purged Xorg while trying to remove KDE 😬️

# <img src="https://www.pardus.org.tr/wp-content/uploads/2019/08/Pardus-04.png" alt="Pardus logo" width=20em height=auto> Pardus

I started my Linux journey with [Pardus](https://www.pardus.org.tr) installation CD distributed with science magazine of [TÜBİTAK](https://www.tubitak.gov.tr/en "Scientific and Technological Research Council of Turkey"). It probably was around 2011. What I do remember is the sluggish KDE 4 desktop that brought old family computer (512MB RAM, 1,6GHz single core CPU) to its knees. What I liked about Pardus was how easy it was to get started thanks to familiar looking KDE. Even though KDE was sluggish on that laptop, compiz animations were fast. WiFi worked out of the box with some occasional disconnection issue. It also had Firefox and no Internet Explorer. I learned one thing with Pardus, Windows is not the only operating system out there and one doesn't have to pirate software to have a functional computer.

2011 was the time I never knew about the terminal. I never searched for it on application manager. Linux desktop was usable without CLI in 2011. Forget about shell commands, I didn't know English language back then. Unfortunately the 7 years old laptop couldn't hold any longer and just died in a sunny day of 2012. It wasn't Linux's problem. Laptop was from 2005.

__6/10__  
Would run it for nostalgia on a VM 😁️

# Hardware
Lenovo IdeaPad __110__  
__AMD A8__ CPU (4 cores boosting to 2.2GHz) with iGPU (for rendering desktop)  
__8GB RAM__ (6.7GB usable thanks to integrated GPU 😠️)  
__AMD R5__ GPU (2GB Vram for projector support)  
__Fan__ for cooling through motivation  
__1366x768__ display (for 720p window + 48 pixel for title and task bars 😅️)  
__Keyboard__ (with short Shift key 😠️) with numberpad  
__Touchpad__ which is comfortable and sometimes makes me forget to use mouse  
__Barely Camera__ because why not  
__Plastic body__ because it worked for Barbie 🤣️
__1TB HDD__ 5200 RPM
__DVD R/W bay__ which I replaced with __240GB SSD__ with on board RAM

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/010/369/original/ubuntu.png" alt="Ubuntu logo" width=20em height=auto> Ubuntu 

I installed Ubuntu on IdeaPad 110 the day it was delivered in 2017. Unity desktop environment was a nice change from other operating systems I used. Ubuntu was amazing and every program I could need was available in the store. WiFi and bluetooth worked amazingly well out of the box.

There were 2 things I didn't like about Ubuntu:  
First is the AppArmor internal error prompt. I disabled AppArmor and issue was fixed 🤦‍♂️️  
Second was the half new and half old looking system after a release upgrade.

__7/10__

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/106/747/original/ab7ee760c4d7d8b4.png" alt="Mint Logo" width=20em height=auto> Linux Mint
<div style="text-align: center;">
    <img src="images/linux_mint_change_my_mind_1.jpg" alt="Linux Mint is Ubuntu plus cinnamon DE minus snap packages. Change my mind">
</div>

__8/10__  
Roll it to earn 1 point

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/090/050/original/ae5ab97b259fe17d.png" alt="Solus Logo" width=20em height=auto> Arch

I learned what RTFM means and how useful Arch wiki is. Nonetheless I ended up with something that wouldn't work. I know it was my mistake to install whatever I could find on AUR which can break the system. But in my defense, I was spoiled by installing stuff from a repo and not breaking anything.

__Some assembly required__ / 10  
I am not [Mad Lad](https://www.jupiterbroadcasting.com/57622/arch-home-server-challenge-las-313/ "Arch Home Server") enough

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/025/128/original/ba24e6fc18d3e34a.png" alt="Solus Logo" width=20em height=auto> Solus 3.99

I tried Solus because my prejudgment for stability of Arch Linux prevented me trying Manjaro. Solus was much faster than Ubuntu even though I tried GNOME edition. Games were playable which is an interesting development given that this laptop is suitable for offices and gaming isn't something it should be able to do.

Every GPU driver update was causing system to boot into black screen. Fix was rebooting it system to safe mod and rebooting to latest driver. I would be happy to use stable kernel instaled of edge but graphics drivers didn't work with stable for some reason. Given Solus is a rolling release, I was getting this problem 2 or 3 times a week.

__███ / 10__

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/025/121/original/81f2041d18d0535b.png" alt="Manjaro logo" width=20em height=auto> Manjaro

It works. It is lighter than anything I tried so far, partly thanks to optimization done to KDE. Manjaro is stable and is recommendable to grandpas. That being said, Manjaro and Arch's one of the biggest attraction is AUR which also is the weak spot. Most people break their systems because they did something wrong while installing from AUR.

__8/10__  
Still recommended though

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/025/123/original/a76f54a7dbcd8afd.png" alt="Pop OS logo" width=20em height=auto> Pop_OS!
Pop_OS! is the Ubuntu++ that I expected Linux Mint to be. Pop is especially recommended for computers with Nvidia's sand. But for some reason it kept slowing down faster than any other OS I tried on this computer (excpet Windows 10 which always was slow).

I started the installation and let it use the entire SSD the way it wished. All the defaults except the encryption which I don't think effects the performance because if it did, why wasn't it affecting at the beginning. About 50GB or 240GB SSD is filled.

I used to distro hop for fun and trying other distros. Not with Pop_OS ! I wanted to use this for the long shot but it keeps slowing down on me.

__9/10__  
Only if it knew it was installed on an SSD and performed so.

# <img src="https://fosstodon.b-cdn.net/custom_emojis/images/000/025/128/original/ba24e6fc18d3e34a.png" alt="Solus Logo" width=20em height=auto> Solus 4.1

It turns out, both iGPU and complementrary dedicated GPU supports Vulkan. No other system could run anything on Vulkan before. Even after adding PPAs and installing `mesa` and `vulkan` stuff, I always had to use `PROTON_USE_WINED3D=1` flag to run windows games. Native games performed poorly too. Solus was like a fresh air to my computer 😃️

GPU driver update no longer causes black screen. I installed it on the HDD to try it out. And with Pop_OS! on SSD slowed down enough to be almost match with Solus on HDD, I am planning to replace Pop with Solus.

__9/10__ until it break again  
Sorry for being negative but if it won't break (hopefully), I will make a newbie mistake at some point and break it 😁️

# This is NOT the end for a distro hopper
I believe the reason geeks started to distro hop is that there are many different people with different needs and there are different distros with strong point and weaknesses. Desire for finding the best for our own use case pushes us to hop between many distros.

- There are developers who need to have the latest libraries to test against their projects. 
- There are grand parents who need a way to communicate with their children and grandchildren.
- There are servers which run day and night to provide services to millions of people.

There is no single best solution. That is actually why there are many distros in the first place. We will keep hopping in hopes of finding the best for our personal use case andbe able to give educated answer to people who ask for recomendation.

<div style="text-align: center;">
    <img src="images/distro_hopping_bullet_cat_1.jpg" alt="A cat hovering off the ground. Top caption: There is a new distro with mouse themed wallpapers. Bottom caption: It is time to hop">
</div>

